from enum import Enum, unique


@unique
class Period(Enum):
    FIVE_MINUTES = 300
    FIFTEEN_MINUTES = 900
    THIRTY_MINUTES = 1800
    TWO_HOURS = 7200
    FOUR_HOURS = 14400
    DAILY = 86400
