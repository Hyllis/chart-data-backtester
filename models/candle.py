from datetime import datetime
import json


class Candle(object):
    def __init__(self, dct): # date, open, high, low, close, volume, quoteVolue, weightedAverage):
        self.date = int(dct['date'])
        self.open = dct['open']
        self.high = dct['high']
        self.low = dct['low']
        self.close = dct['close']
        self.volume = dct['volume']
        self.quoteVolume = dct['quoteVolume']
        self.weightedAverage = dct['weightedAverage']

    @staticmethod
    def tuple_to_dict(tuple):
        dct = {}
        dct['date'] = int(tuple[0].timestamp())
        dct['open'] = tuple[1]
        dct['high'] = tuple[2]
        dct['low'] = tuple[3]
        dct['close'] = tuple[4]
        dct['volume'] = tuple[5]
        dct['quoteVolume'] = tuple[6]
        dct['weightedAverage'] = tuple[7]
        return dct

    def show(self):
        print(str(datetime.fromtimestamp(self.date)) + " - hello i'm " + str(self.date) + " open " + str(self.open) + " volume " + str(self.volume))
