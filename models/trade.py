from enum import Enum, unique, auto


class Trade:
    def __init__(self, date, entry, trade_type, stop_loss, take_profit):
        self.date = date
        self.entry = entry
        self.trade_type = trade_type
        self.stop_loss = stop_loss
        self.take_profit = take_profit

    def as_dict(self):
        return {
            'date': self.date,
            'type': self.trade_type.name,
            'entry': self.entry,
            'stop_loss': self.stop_loss,
            'take_profit': self.take_profit,
        }


@unique
class TradeType(Enum):
    LONG = auto()
    SHORT = auto()
