from models.currency import Currency


class CurrencyPair:
    def __init__(self, base: Currency, target: Currency):
        self.base = base
        self.target = target

    def base(self):
        return self.base

    def target(self):
        return self.target
