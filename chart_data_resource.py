from flask import Flask, request
from flask_cors import CORS, cross_origin
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from models.period import Period
from models.currency import Currency
from chart_data import ChartData
import jsons
from flask_jsonpify import jsonify


db_connect = create_engine('sqlite:///chinook.db')
app = Flask(__name__)
api = Api(app)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

cache = {}


class Candles(Resource):
    def get(self, base: str, target: str, period: str, page_id: int):
        key = base + "_" + target + "_" + period
        if key not in cache:
            cache[key] = ChartData(Currency[base], Currency[target], Period[period])
        chart_data = cache[key]
        return jsonify(chart_data.get_candles_page(page_id))


class Indicator(Resource):
    def get(self, base: str, target: str, period: str, indicator: str, page_id: int):
        key = base + "_" + target + "_" + period
        if key not in cache:
            cache[key] = ChartData(Currency[base], Currency[target], Period[period])
        chart_data = cache[key]
        return jsonify(chart_data.get_indicator_data_page(indicator, page_id))


class AvaibleCurrencyPairs(Resource):
    def get(self):
        return [
            {'base': Currency.USDT.name, 'target': Currency.BTC.name},
            {'base': Currency.USDT.name, 'target': Currency.ETH.name},
            {'base': Currency.USDT.name, 'target': Currency.LTC.name},
            {'base': Currency.USDT.name, 'target': Currency.TRX.name},
            {'base': Currency.USDT.name, 'target': Currency.ZEC.name},
            {'base': Currency.USDT.name, 'target': Currency.BTT.name},
            {'base': Currency.BTC.name, 'target': Currency.ETH.name},
            {'base': Currency.BTC.name, 'target': Currency.LTC.name},
            {'base': Currency.BTC.name, 'target': Currency.XRP.name},
            {'base': Currency.BTC.name, 'target': Currency.XMR.name},
            {'base': Currency.BTC.name, 'target': Currency.BCHABC.name},
            {'base': Currency.ETH.name, 'target': Currency.ETC.name},
            {'base': Currency.USDT.name, 'target': Currency.BULL.name},
            {'base': Currency.USDT.name, 'target': Currency.BEAR.name},
            {'base': Currency.USDT.name, 'target': Currency.ETHBULL.name},
            {'base': Currency.USDT.name, 'target': Currency.ETHBEAR.name},
        ]

api.add_resource(Candles, '/chartdata/<base>/<target>/<period>/candles/page/<page_id>')
api.add_resource(Indicator, '/chartdata/<base>/<target>/<period>/indicator/<indicator>/page/<page_id>')
api.add_resource(AvaibleCurrencyPairs, '/chartdata/pairs')

if __name__ == '__main__':
    app.run(port='5365')
