from indicators.candles.volume import Volume
from indicators.candles.moving_average import MovingAverage
from indicators.candles.exponential_moving_average import ExponentialMovingAverage
from indicators.candles.standard_deviation import StandardDeviation
from indicators.candles.bollinger_bands import BollingerBands
from indicators.candles.macd import MACD
from strategies.strategy_executor import StrategyExecutor
from indicators.candles.true_range import TrueRange
from indicators.candles.average_true_range import AverageTrueRange
from indicators.candles.period_min_max import PeriodMinMax
from strategies.macd_crossover import MACDCrossover
from strategies.volatility_detector import VolatilityDetector
from indicators.candles.alligator import Alligator
from indicators.buffer import Buffer
from indicators.candles.pattern_detector import PatternDetector
from indicators.candles.rsi import RSI


class Factory:
    indicators = [
        Volume,
        MovingAverage,
        ExponentialMovingAverage,
        StandardDeviation,
        BollingerBands,
        MACD,
        StrategyExecutor,
        TrueRange,
        AverageTrueRange,
        PeriodMinMax,
        MACDCrossover,
        VolatilityDetector,
        Alligator,
        Buffer,
        PatternDetector,
        RSI,
    ]

    @staticmethod
    def indicator_from_type(indicator_type: str):
        filtered = list(filter(lambda indicator: indicator.ontology()['type'] == indicator_type, Factory.indicators))
        if len(filtered) != 1:
            raise Exception("Unknown indicator: {}".format(indicator_type))
        return filtered[0]

    @staticmethod
    def get(indicator: str, indicators_cache):
        params = indicator.split("_")
        indicator_type = params.pop(0)
        if len(params) == 1 and len(params[0]) == 0:
            params = []  # no params
        return Factory.indicator_from_type(indicator_type)(params)
