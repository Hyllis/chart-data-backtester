from indicators.indicator import Indicator


class Buffer(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'Buffer',
            'params': [
                {'name': 'length', 'type': int, 'default': 5, 'range': {'min': 1}},
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.window = []
        print("ini widow - " + str(len(self.window)))

    def next(self, value):
        self.i += 1
        self.window.append(value)
        if len(self.window) > self.params.length:
            self.window.pop(0)

        self.current = {'values': self.window.copy()}
        return {'values': self.window.copy()}
