from abc import abstractmethod
from indicators.indicator_params import IndicatorParams
import time


class Indicator:
    @abstractmethod
    def ontology(self):
        pass

    @abstractmethod
    def next(self, candle):
        pass

    def current(self):
        return self.current

    def finish(self):
        self.debug_show()

    def get_data(self, candles):
        self.time_start = time.time()
        data = []
        for candle in candles:
            data.append(self.next(candle))
        self.finish()
        return data

    def get_data_as_list(self, candles, key):
        return list(map(lambda v: v[key], self.get_data(candles)))

    def params_ontology_to_string(self):
        return ', '.join(list(map(lambda o: o['name'] + ': ' + o['type'].__name__ + ' (' + str(o['default']) + ')',
                                  self.ontology()['params'])))

    def type_check(self, type, value, name):
        try:
            return type(value)
        except:
            raise ValueError(self.ontology()['name'] + " - " + name + " should be of type " + type.__name__ +
                             " - couldn't cast " + str(value))

    def value_check(self, range, value, name):
        if 'values' in range and value not in range['values']:
            raise ValueError(self.ontology()['name'] + " - " + name + " should be in " + str(range['values']) +
                             " - got " + str(value))
        if 'min' in range and value < range['min']:
            raise ValueError(self.ontology()['name'] + " - minimum for " + name + " is " + str(range['min']) +
                             " - got " + str(value))
        if 'max' in range and value > range['max']:
            raise ValueError(self.ontology()['name'] + " - maximum for " + name + " is " + str(range['max']) +
                             " - got " + str(value))

    def debug_show(self):
        name = self.ontology()['name']
        params_names = list(map(lambda p: p['name'], self.ontology()['params']))
        params = self.raw_params
        acc = []
        i = 0
        while i < len(params):
            acc.append([params_names[i], params[i]])
            i += 1
        elapsed = time.time() - self.time_start
        acc.append(['elapsed', "{:.3f}s".format(elapsed)])
        acc = list(map(lambda t: "\033[35m{}\033[0m = \033[1m{}\033[0m".format(t[0], t[1]), acc))
        print("\033[34;1m{}\033[0m {}".format(name, ", ".join(acc)))

    def __init__(self, params):
        self.data = []
        self.current = {}
        self.raw_params = params
        self.time_start = time.time()
        ontology = self.ontology()
        params_ontology = ontology['params']
        if len(params) != len(params_ontology):
            raise Exception(
                ontology['name'] + " - got " + str(params) + " - expected " + self.params_ontology_to_string())
        self.params = IndicatorParams()
        i = 0
        while i < len(params_ontology):
            p = self.type_check(params_ontology[i]['type'], params[i], params_ontology[i]['name'])
            if 'range' in params_ontology[i]:
                self.value_check(params_ontology[i]['range'], p, params_ontology[i]['name'])
            setattr(self.params, params_ontology[i]['name'], p)
            i += 1
