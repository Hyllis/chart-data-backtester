from indicators.indicator import Indicator


class PeriodMinMaxValues(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'MinMaxV',
            'params': [
                {'name': 'length', 'type': int, 'default': 100, 'range': {'min': 1}},
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.window_min = []
        self.window_max = []

    def next(self, low_high):
        self.i += 1
        self.window_min.append(low_high[0])
        self.window_max.append(low_high[1])
        window_min = [i for i in self.window_min if i]
        window_max = [i for i in self.window_max if i]
        if len(self.window_min) >= self.params.length:
            self.window_min.pop(0)
            self.window_max.pop(0)
        elem = {
            'min': 0 if len(window_min) == 0 else min(window_min),
            'max': 0 if len(window_max) == 0 else max(window_max),
        }
        self.current = elem.copy()
        return elem
