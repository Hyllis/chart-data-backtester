from indicators.indicator import Indicator
from indicators.values.moving_average_values import MovingAverageValues


class ExponentialMovingAverageValues(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'EMAV',
            'params': [
                {'name': 'length', 'type': int, 'default': 9, 'range': {'min': 1}},
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.multiplier = 2 / (self.params.length + 1)
        self.mav = MovingAverageValues([self.params.length])
        self.prev_mav = None
        self.prev_ema = None

    def next(self, value):
        self.i += 1
        if self.prev_mav is None:
            self.prev_mav = self.mav.next(value)['value']
            cur_ema = None
        else:
            # could work without MA by replacing self.ma[i-1] by self.candles[i-1] (a bit less accurate at first)
            prev = self.prev_ema if self.prev_ema is not None else self.prev_mav
            cur_ema = (value * self.multiplier) + (prev * (1 - self.multiplier))
        self.prev_ema = cur_ema
        elem = {'value': cur_ema}
        self.current = elem.copy()
        return elem
