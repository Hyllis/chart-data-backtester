from indicators.indicator import Indicator


class MovingAverageValues(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'MAV',
            'params': [
                {'name': 'length', 'type': int, 'default': 9, 'range': {'min': 1}},
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.window = []

    def next(self, value):
        self.i += 1
        self.window.append(value)
        if len(self.window) < self.params.length:
            elem = {'value': None}
        else:
            if None in self.window:
                self.window.pop(0)
                elem = {'value': None}
            else:
                avg = sum(self.window) / len(self.window)
                self.window.pop(0)
                elem = {'value': avg}
        self.current = elem.copy()
        return elem
