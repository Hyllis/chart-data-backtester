from indicators.indicator import Indicator
from indicators.values.exponential_moving_average_values import ExponentialMovingAverageValues
from indicators.values.period_min_max_values import PeriodMinMaxValues


class Volume(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'Volume',
            'params': [
                {'name': 'length', 'type': int, 'default': 9, 'range': {'min': 1}},
                {'name': 'min_max_length', 'type': int, 'default': 200, 'range': {'min': 1}},
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.mav = ExponentialMovingAverageValues([self.params.length])
        self.mav_slow = ExponentialMovingAverageValues([self.params.min_max_length])

    def next(self, candle):
        self.i += 1
        mav = self.mav.next(candle['volume'])['value']
        mav_slow = self.mav_slow.next(candle['volume'])['value']
        # min_max = self.min_max.next((avg, avg))
        # ratioed = min_max['min'] + (min_max['max'] - min_max['min']) * 0.7
        agitation = candle['volume'] if mav is not None and mav_slow is not None and mav > mav_slow else None
        return {'value': candle['volume'], 'slow': mav_slow, 'fast': mav, 'agitation': agitation}
