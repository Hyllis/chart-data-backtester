from indicators.indicator import Indicator


class TrueRange(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'TR',
            'params': [
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.prev_close = 0

    def next(self, candle):
        self.i += 1
        a = abs(candle['high'] - self.prev_close)
        b = abs(candle['low'] - self.prev_close)
        c = candle['high'] - candle['low']
        self.prev_close = candle['close']
        elem = {'value': max(a, b, c)}
        self.current = elem.copy()
        return elem
