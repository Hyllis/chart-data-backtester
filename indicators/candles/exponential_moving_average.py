from indicators.indicator import Indicator
from indicators.values.exponential_moving_average_values import ExponentialMovingAverageValues


class ExponentialMovingAverage(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'EMA',
            'params': [
                {'name': 'length', 'type': int, 'default': 9, 'range': {'min': 1}},
                {'name': 'mode', 'type': str, 'default': 'close', 'range': {'values': ['open', 'high', 'low', 'close', 'weightedAverage']}},
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.emav = ExponentialMovingAverageValues([self.params.length])

    def next(self, candle):
        self.i += 1
        elem = self.emav.next(candle[self.params.mode])
        self.current = elem.copy()
        return elem
