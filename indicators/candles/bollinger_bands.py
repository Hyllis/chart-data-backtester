from indicators.indicator import Indicator
from indicators.values.moving_average_values import MovingAverageValues
from indicators.candles.standard_deviation import StandardDeviation


class BollingerBands(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'BB',
            'params': [
                {'name': 'length', 'type': int, 'default': 20, 'range': {'min': 1}},
                {'name': 'multiplier', 'type': int, 'default': 2, 'range': {'min': 1}},
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.mode = 'close'
        self.mav = MovingAverageValues([self.params.length])
        self.sd = StandardDeviation([self.params.length, self.mode])
        self.current = {}

    def next(self, candle):
        self.i += 1
        ma = self.mav.next(candle[self.mode])['value']
        sd = self.sd.next(candle)['value']
        elem = {'middle': ma, 'upper': None, 'lower': None}
        if ma is not None and sd is not None:
            elem['upper'] = ma + (sd * self.params.multiplier)
            elem['lower'] = ma - (sd * self.params.multiplier)
        self.current = elem.copy()
        return elem
