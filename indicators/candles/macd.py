from indicators.indicator import Indicator
from indicators.values.exponential_moving_average_values import ExponentialMovingAverageValues


class MACD(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'MACD',
            'params': [
                {'name': 'length_fast', 'type': int, 'default': 12, 'range': {'min': 1}},
                {'name': 'length_slow', 'type': int, 'default': 26, 'range': {'min': 1}},
                {'name': 'mode', 'type': str, 'default': 'close', 'range': {'values': ['open', 'high', 'low', 'close']}},
                {'name': 'length_signal', 'type': int, 'default': 9, 'range': {'min': 1}},
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.ema_fast = ExponentialMovingAverageValues([self.params.length_fast])
        self.ema_slow = ExponentialMovingAverageValues([self.params.length_slow])
        self.ema_signal = ExponentialMovingAverageValues([self.params.length_signal])

    def next(self, candle):
        self.i += 1

        ema_fast = self.ema_fast.next(candle[self.params.mode])['value']
        ema_slow = self.ema_slow.next(candle[self.params.mode])['value']
        macd = None if ema_fast is None or ema_slow is None else ema_fast - ema_slow
        signal = self.ema_signal.next(macd)['value']
        histogram = None if macd is None or signal is None else macd - signal

        elem = {'macd': macd, 'signal': signal, 'histogram': histogram}
        self.current = elem.copy()
        return elem
