from indicators.indicator import Indicator
import math


class StandardDeviation(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'StdDev',
            'params': [
                {'name': 'length', 'type': int, 'default': 9, 'range': {'min': 1}},
                {'name': 'mode', 'type': str, 'default': 'close', 'range': {'values': ['open', 'high', 'low', 'close']}},
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.window = []
        super().debug_show()

    def next(self, candle):
        self.i += 1
        self.window.append(candle[self.params.mode])
        if len(self.window) < self.params.length:
            value = None
        else:
            value = StandardDeviation.std(self.window)
            self.window.pop(0)
        elem = {'value': value}
        self.current = elem.copy()
        return elem

    @staticmethod
    def std(values):
        mean = sum(values) / len(values)
        differences = list(map(lambda v: (v - mean) * (v - mean), values))
        sum_diff = 0
        for v in differences:
            sum_diff += v
        variance = sum_diff / len(values)
        return math.sqrt(variance)
