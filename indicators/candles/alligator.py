from indicators.indicator import Indicator
from indicators.values.exponential_moving_average_values import ExponentialMovingAverageValues


class Alligator(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'Alligator',
            'params': [
                {'name': 'jaw_length', 'type': int, 'default': 21, 'range': {'min': 1}},  # offset 8
                {'name': 'teeth_length', 'type': int, 'default': 13, 'range': {'min': 1}},  # offset 5
                {'name': 'lips_length', 'type': int, 'default': 8, 'range': {'min': 1}},  # offset 3
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.mode = 'weightedAverage'
        self.jaw = ExponentialMovingAverageValues([self.params.jaw_length])
        self.teeth = ExponentialMovingAverageValues([self.params.teeth_length])
        self.lips = ExponentialMovingAverageValues([self.params.lips_length])

    def next(self, candle):
        self.i += 1
        jaw = self.jaw.next(candle[self.mode])['value']
        teeth = self.jaw.next(candle[self.mode])['value']
        lips = self.jaw.next(candle[self.mode])['value']
        elem = {'jaw': jaw, 'teeth': teeth, 'lips': lips}
        self.current = elem.copy()
        return elem
