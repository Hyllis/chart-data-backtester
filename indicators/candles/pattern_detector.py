from indicators.indicator import Indicator
from indicators.buffer import Buffer
from candlestick_patterns.pattern_factory import Pattern
from indicators.values.exponential_moving_average_values import ExponentialMovingAverageValues


class PatternDetector(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'PatternDetector',
            'params': [
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.buffer = Buffer([5])
        self.ema = ExponentialMovingAverageValues([200])
        self.stats = {}

    def finish(self):
        pass
        # for k in self.stats.keys():
        #     print("- {}: {} ({:.2f}%)".format(k, self.stats[k], self.stats[k] / self.i * 100))
        # print("- Total: {}".format(self.i))

    def next(self, candle):
        self.i += 1
        ema = self.ema.next(candle['close'])['value']
        candles = self.buffer.next(candle)['values']
        trend = 0
        if ema is not None and candle['low'] > ema:
            trend = 1
        elif ema is not None and candle['high'] < ema:
            trend = -1
        ret = Pattern.detect_any(trend, candles)
        if 'detected' in ret:
            k = ret['detected']['direction'] + "_" + ret['detected']['name']
            if k not in self.stats:
                self.stats[k] = 0
            self.stats[k] += 1
        return ret
