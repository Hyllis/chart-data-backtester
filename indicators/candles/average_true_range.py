from indicators.indicator import Indicator
from indicators.candles.true_range import TrueRange
from indicators.values.moving_average_values import MovingAverageValues
from indicators.values.exponential_moving_average_values import ExponentialMovingAverageValues


class AverageTrueRange(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'ATR',
            'params': [
                {'name': 'length', 'type': int, 'default': 14, 'range': {'min': 1}},
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.tr = TrueRange([])
        self.mav = MovingAverageValues([self.params.length])

    def next(self, candle):
        self.i += 1
        tr = self.tr.next(candle)['value']
        atr = self.mav.next(tr)['value']
        elem = {'value': atr, 'tr': tr}
        self.current = elem.copy()
        return elem
