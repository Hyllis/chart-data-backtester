from indicators.indicator import Indicator
from indicators.values.moving_average_values import MovingAverageValues


# TODO fix (close, but not exactly the same RSI value as Poloniex's)

class RSI(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'RSI',
            'params': [
                {'name': 'length', 'type': int, 'default': 14, 'range': {'min': 1}},
                {'name': 'top', 'type': int, 'default': 70, 'range': {'min': 0, 'max': 100}},
                {'name': 'bottom', 'type': int, 'default': 30, 'range': {'min': 0, 'max': 100}},
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.multiplier = 2 / (self.params.length + 1)
        self.mav_gain = MovingAverageValues([self.params.length])
        self.mav_loss = MovingAverageValues([self.params.length])
        self.prev_gain = None
        self.prev_loss = None
        self.prev_candle = None

    def next(self, candle):
        self.i += 1
        avg_gain = None
        avg_loss = None
        smoothed_gain = None
        smoothed_loss = None
        if self.prev_candle is not None:
            change = abs(candle['close'] - self.prev_candle['close'])
            is_gain = candle['close'] > self.prev_candle['close']
            cur_gain = 0 if not is_gain else change
            cur_loss = 0 if is_gain else change
            avg_gain = self.mav_gain.next(cur_gain)['value']
            avg_loss = self.mav_loss.next(cur_loss)['value']
            # print("Change {} - is {} - gain {} - loss {} -- prev gain {}".format(change, is_gain, gain, loss, self.prev_gain))
            if self.prev_gain is not None:
                smoothed_gain = (self.prev_gain * (self.params.length - 1) + cur_gain) / self.params.length
                smoothed_loss = (self.prev_loss * (self.params.length - 1) + cur_loss) / self.params.length
            else:
                smoothed_gain = avg_gain
                smoothed_loss = avg_loss
        self.prev_candle = candle
        self.prev_gain = avg_gain
        self.prev_loss = avg_loss
        # print("gain {} - loss {} -- prev gain {}".format(gain, loss, self.prev_gain))

        rs = None if smoothed_gain is None else smoothed_gain / (smoothed_loss + 0.00000001)
        rsi = None if rs is None else 100 - (100 / (1 + rs))
        return {'rsi': rsi, 'rs': rs, 'top': self.params.top, 'bottom': self.params.bottom}
