from indicators.indicator import Indicator
from indicators.values.period_min_max_values import PeriodMinMaxValues


class PeriodMinMax(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'MinMax',
            'params': [
                {'name': 'length', 'type': int, 'default': 100, 'range': {'min': 1}},
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.min_max = PeriodMinMaxValues([self.params.length])

    def next(self, candle):
        self.i += 1
        elem = self.min_max.next((candle['low'], candle['high']))
        self.current = elem.copy()
        return elem
