from indicators.indicator import Indicator
from indicators.values.moving_average_values import MovingAverageValues


class MovingAverage(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'MA',
            'params': [
                {'name': 'length', 'type': int, 'default': 9, 'range': {'min': 1}},
                {'name': 'mode', 'type': str, 'default': 'close', 'range': {'values': ['open', 'high', 'low', 'close', 'weightedAverage']}},
                # {'name': 'offset', 'type': int, 'default': 0, 'range': {'min': 0}},
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        self.i = -1
        self.mav = MovingAverageValues([self.params.length])

    def next(self, candle):
        self.i += 1
        elem = self.mav.next(candle[self.params.mode])
        self.current = elem.copy()
        return elem
