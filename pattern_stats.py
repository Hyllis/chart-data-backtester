from models.period import Period
from models.currency import Currency
from chart_data import ChartData
import time

class PatternStats:
    RANGE_SEARCH = 15
    # PATTERN_NAMES = ["BULLISH_MegaHammer", "BULLISH_BigHammer", "BULLISH_StrictHammer", "BULLISH_Hammer", "BULLISH_InvertedHammer", "BULLISH_MorningStar", "BULLISH_BULLISH_Engulfing", "BULLISH_ThreeWhiteSoldiers", "BEARISH_MegaHangingMan", "BEARISH_BigHangingMan", "BEARISH_StrictHangingMan", "BEARISH_HangingMan", "BEARISH_ShootingStar", "BEARISH_EveningStar", "BEARISH_BearishEngulfing", "BEARISH_ThreeBlackCrows"]
    PATTERN_NAMES = ["MegaHammer", "BigHammer", "StrictHammer", "Hammer", "InvertedHammer", "MorningStar", "BullishEngulfing", "ThreeWhiteSoldiers", "MegaHangingMan", "BigHangingMan", "StrictHangingMan", "HangingMan", "ShootingStar", "EveningStar", "BearishEngulfing", "ThreeBlackCrows"]

    def __init__(self, base: Currency, target: Currency, period: Period):
        self.meta_stats = {}
        self.base = base
        self.target = target
        self.period = period
        self.chart_data = ChartData(base, target, period)
        self.start_time = time.time()
        self.candles = self.chart_data.get_candles()
        self.patterns = self.chart_data.get_indicator_data('PatternDetector')
        i = 0
        while i < len(self.candles) - PatternStats.RANGE_SEARCH:
            if 'detected' in self.patterns[i]:
                candle = self.candles[i]
                pattern = self.patterns[i]['detected']
                # print("Pattern {} detected at {} on candle {}".format(pattern['name'], i, candle))
                stats = self.get_stats_from(i)
                self.add_to_meta_stats(pattern['name'], pattern['direction'], stats)
            i += 1
        self.end_time = time.time()
        self.finalize_meta_stats()

    def get_stats_from(self, start_id):
        close = self.candles[start_id]['close']
        min_close = 9999999
        max_close = -999999
        range_ratios = []
        range_nbs = []
        i = start_id + 1
        while i <= start_id + PatternStats.RANGE_SEARCH:
            candle = self.candles[i]
            range_nbs.append(candle['close'])
            range_ratios.append(candle['close'] / close)
            min_close = min(min_close, candle['low'])
            max_close = max(max_close, candle['high'])
            i += 1
        # print("Min {}, max {}, range {}".format(min_close, max_close, range_nbs))
        # print("Min {}, max {}, range {}".format(max_close / close, min_close / close, range_ratios))
        return {
            'max': max_close / close,
            'min': min_close / close,
            'range': range_ratios,
        }
    
    def add_to_meta_stats(self, pattern_name, direction, stats):
        if pattern_name not in self.meta_stats:
            self.meta_stats[pattern_name] = {'count': 0, 'avg_min': 0, 'avg_max': 0, 'avg_range': [0] * PatternStats.RANGE_SEARCH}
        meta = self.meta_stats[pattern_name]
        meta['direction'] = direction
        meta['count'] += 1
        meta['avg_min'] += stats['min']
        meta['avg_max'] += stats['max']
        i = 0
        while i < PatternStats.RANGE_SEARCH:
            meta['avg_range'][i] += stats['range'][i]
            i += 1
    
    def finalize_meta_stats(self):
        for k in self.meta_stats.keys():
            meta = self.meta_stats[k]
            meta['elapsed'] = self.end_time - self.start_time
            meta['detection'] = meta['count'] / len(self.candles) * 1
            meta['avg_min'] /= meta['count']
            meta['avg_min'] = "{:.8f}".format((meta['avg_min'] - 1) * 1)
            meta['avg_max'] /= meta['count']
            meta['avg_max'] = "{:.8f}".format((meta['avg_max'] - 1) * 1)
            i = 0
            while i < PatternStats.RANGE_SEARCH:
                meta['avg_range'][i] /= meta['count']
                meta['avg_range'][i] = "{:.8f}".format((meta['avg_range'][i] - 1) * 1)
                i += 1
    
    def display_stats_header(self):
        print("Elapsed s, Base, Target, Period, Pattern name, Direction, Detection rate, Detected, Total count, Min, Max, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20")

    def display_stats_for_pattern(self, pattern_name):
        f = "{:.2f}, {}, {}, {}, {}, {}, {:.6f}, {}, {}, {}, {}, {}"
        if pattern_name in self.meta_stats:
            meta = self.meta_stats[pattern_name]
            print(f.format(self.end_time - self.start_time, self.base.name, self.target.name, self.period.name, pattern_name, meta['direction'],
                           meta['detection'], meta['count'], len(self.candles),
                           meta['avg_min'], meta['avg_max'], ', '.join(meta['avg_range'])))
        else:
            print(f.format(self.end_time - self.start_time, self.base.name, self.target.name, self.period.name, pattern_name, '?',
                           0, 0, len(self.candles),
                           ' ', ' ', ' '))


ps = PatternStats(Currency.USDT, Currency.TRX, Period.TWO_HOURS)
ps.display_stats_header()
for name in PatternStats.PATTERN_NAMES:
    ps.display_stats_for_pattern(name)

