from indicators.indicator import Indicator
from indicators.candles.exponential_moving_average import ExponentialMovingAverage
from indicators.candles.macd import MACD
from strategies import TradingActions


class MACDCrossoverLegacy(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'MACDCrossover',
            'params': [
                # {'name': 'length_fast', 'type': int, 'default': 12, 'range': {'min': 1}},
                # {'name': 'length_slow', 'type': int, 'default': 26, 'range': {'min': 1}},
                # {'name': 'mode', 'type': str, 'default': 'close', 'range': {'values': ['open', 'high', 'low', 'close']}},
                # {'name': 'length_signal', 'type': int, 'default': 9, 'range': {'min': 1}},
            ],
        }

    def __init__(self, params, candles):
        super().__init__(params)
        self.risk_ratio = 1.4
        self.period_check = 35
        self.candles = candles
        self.ema_trend = ExponentialMovingAverage([200, 'close'], candles).get_data_as_list('value')
        self.macd = MACD([12, 26, 'close', 9], candles).get_data()
        self.data = self.build_data()
        super().debug_show()

    def build_data(self):
        data = [{'entry': None}]
        trade = TradingActions(self.ontology()['name'],  self.risk_ratio, 10000)
        prev_macd_above = True
        trend_up = False
        i = 1
        non_null = 0
        won_trade = 0
        max_streak_win = 0
        max_streak_lose = 0
        period_maxes = []
        period_mins = []
        streak_win = 0
        streak_lose = 0
        wallet = 10000
        in_trade_until = 0
        while i < len(self.candles) - 1:
            elem = {}
            macd_above = True
            if self.ema_trend[i] is not None and self.ema_trend[i - 1] is not None:
               # trend_up = self.ema_trend[i - 1] < self.ema_trend[i]
                trend_up = self.candles[i+1]['weightedAverage'] > self.ema_trend[i]
            if self.macd[i]['macd'] is not None and self.macd[i]['signal'] is not None:
                macd_above = self.macd[i]['macd'] > self.macd[i]['signal']
            if trend_up and i > in_trade_until:
                if self.macd[i]['histogram'] is not None and self.macd[i]['macd'] is not None:
                    if self.macd[i]['macd'] < 0:  # self.macd[i]['histogram']:
                        if macd_above and not prev_macd_above:
                            elem['entry'] = self.candles[i+1]['weightedAverage']
                            diff = self.ema_trend[i] - self.candles[i]['close']
                            # elem['stop_loss'] = self.candles[i]['close'] - diff
                            # elem['take_profit'] = self.candles[i]['close'] + diff * 1.4
                            elem['stop_loss'] = self.ema_trend[i]
                            elem['take_profit'] = elem['entry'] + (elem['entry'] - elem['stop_loss']) * self.risk_ratio
                            # elem['status'] = self.check_trade_status(i, elem['stop_loss'], elem['take_profit'])
                            # if elem['stop_loss'] > elem['take_profit']:
                            #     print("FUUU - " + str(self.candles[i]['date']) + " - " + str(elem['entry']) + " - " + str(elem['stop_loss']) + " - " + str(elem['take_profit']))
                            # print("date: " + str(self.candles[i]['date']))
                            # trade.register_trade(elem['entry'], elem['stop_loss'], elem['take_profit'], elem['status'])
                            # # elem['won'] = status['won']
                            # # elem['after'] = status['after']
                            # period_maxes.append((elem['status']['max_period'] / elem['entry'] - 1) * 100)
                            # period_mins.append((elem['status']['min_period'] / elem['entry'] - 1) * 100)
                            # in_trade_until = i + elem['status']['after']
                            # if elem['status']['won']:
                            #     wallet += 100 * self.risk_ratio
                            #     won_trade += 1
                            #     streak_lose = 0
                            #     streak_win += 1
                            #     if streak_win > max_streak_win:
                            #         max_streak_win = streak_win
                            # else:
                            #     wallet -= 100
                            #     streak_win = 0
                            #     streak_lose += 1
                            #     if streak_lose > max_streak_lose:
                            #         max_streak_lose = streak_lose
                            non_null += 1
            data.append(elem)
            prev_macd_above = macd_above
            i += 1
        trade.finish()
        print("- \033[1mAverage period maxes:\033[0m {}".format(0 if len(period_maxes) == 0 else sum(period_maxes) / len(period_maxes)))
        print("- \033[1mAverage period mins:\033[0m {}".format(0 if len(period_mins) == 0 else sum(period_mins) / len(period_mins)))
        return data

    def check_trade_status(self, idx, stop_loss, take_profit):
        status = {'won': None, 'after': 0, 'min_period': 9999999, 'max_period': 0, 'min_period_after': 0, 'max_period_after': 0}
        i = idx + 1
        while i < len(self.candles):
            if self.candles[i]['high'] > take_profit:
                status['won'] = True
                break
            elif self.candles[i]['low'] < stop_loss:
                status['won'] = False
                break
            i += 1
        status['after'] = i - idx

        i = idx + 1
        while i < idx + status['after'] and i < len(self.candles):
            if self.candles[i]['high'] > status['max_period']:
                status['max_period'] = self.candles[i]['high']
                status['max_period_after'] = i - idx
            elif self.candles[i]['low'] < status['min_period']:
                status['min_period'] = self.candles[i]['low']
                status['min_period_after'] = i - idx
            i += 1
        return status
