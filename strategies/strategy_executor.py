from indicators.indicator import Indicator
from strategies.macd_crossover import MACDCrossover
from strategies.bollinger import Bollinger
from strategies.trading_actions import TradingActions


class StrategyExecutor(Indicator):
    strategies = [
        MACDCrossover,
        Bollinger,
    ]

    @staticmethod
    def strategy_from_type(indicator_type: str):
        filtered = list(filter(lambda indicator: indicator.ontology()['type'] == indicator_type, StrategyExecutor.strategies))
        if len(filtered) != 1:
            raise Exception("Unknown indicator: {}".format(indicator_type))
        return filtered[0]

    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'StrategyExecutor',
            'params': [
                {'name': 'strategy_id', 'type': str, 'default': 'MACDCrossover'},
                {'name': 'strategy_params', 'type': str, 'default': '1.5'},
            ],
        }

    def next(self, candle):
        self.i += 1
        trade = self.strategy.next(candle)
        for (start_i, ongoing_trade) in self.ongoing_trades:
            trade = self.strategy.update_trade(candle, ongoing_trade)
            if ongoing_trade['type'] == 'LONG' and (candle['high'] >= ongoing_trade['take_profit'] or candle['low'] <= ongoing_trade['stop_loss']):
                print("Out {} - low {} high {} - tp {} sl {}".format(ongoing_trade['type'], candle['low'], candle['high'], ongoing_trade['take_profit'], ongoing_trade['stop_loss']))
                ongoing_trade['won'] = candle['high'] >= ongoing_trade['take_profit']
            if ongoing_trade['type'] == 'SHORT' and (candle['low'] <= ongoing_trade['take_profit'] or candle['high'] >= ongoing_trade['stop_loss']):
                print("Out {} - low {} high {} - tp {} sl {}".format(ongoing_trade['type'], candle['low'], candle['high'], ongoing_trade['take_profit'], ongoing_trade['stop_loss']))
                ongoing_trade['won'] = candle['low'] <= ongoing_trade['take_profit']
            if 'won' in ongoing_trade and ongoing_trade['won']:
                ongoing_trade['after'] = self.i - start_i
                at = ongoing_trade['take_profit'] if ongoing_trade['won'] else ongoing_trade['stop_loss']
                self.trading.sell(at, ongoing_trade, ongoing_trade['sell_ratio'])

                if ongoing_trade['finalized']:
                    profit = ongoing_trade['end_wallet'] - ongoing_trade['start_wallet']
                    print("candle daet: {}".format(candle['date']))
                    self.trading.log(ongoing_trade['won_global'], profit, ongoing_trade['entry'], ongoing_trade['stop_loss'],
                                     ongoing_trade['take_profit'])
                    self.won_trades += 1 if ongoing_trade['won_global'] else 0
                    # if ongoing_trade['won_global']:
                    #     # TODO WTF, le chiffre n'est pas le même à chaque try
                    #     self.macd_on_won.append(ongoing_trade['macd'] / ongoing_trade['entry'] * 100)
                    # else:
                    #     self.macd_on_lost.append(ongoing_trade['macd'] / ongoing_trade['entry'] * 100)
                    self.trades_count += 1
                self.ongoing_trades.remove((start_i, ongoing_trade))
        if 'entry' in trade and len(self.ongoing_trades) == 0:
            self.trading.buy(trade['entry'], trade['stop_loss'], trade['take_profit'], trade, trade['buy_ratio'])
            self.ongoing_trades.append((self.i, trade))
        return trade

    def finish(self):
        print("{} - {}/{} ({:.2f}%)".format(self.ontology()['name'], self.won_trades, self.trades_count, 0 if self.trades_count == 0 else self.won_trades / self.trades_count * 100))
        print("On win: {} with {} values".format(0 if len(self.macd_on_won) == 0 else sum(self.macd_on_won) / len(self.macd_on_won), len(self.macd_on_won)))
        print("On lost: {} with {} values".format(0 if len(self.macd_on_lost) == 0 else sum(self.macd_on_lost) / len(self.macd_on_lost), len(self.macd_on_lost)))
        self.trading.finish()

    def __init__(self, params):
        super().__init__(params)
        strategy_params = self.params.strategy_params.split("|")
        print("Strategy: {}".format(self.params.strategy_id))
        print("Strategy parasm: " + str(self.params.strategy_params) + " - " + str(len(strategy_params)))
        self.strategy = StrategyExecutor.strategy_from_type(self.params.strategy_id)(strategy_params)
        self.i = -1
        self.trading = TradingActions(self.strategy.ontology()['name'], self.strategy.params.risk_ratio, 100)
        self.ongoing_trades = []
        self.won_trades = 0
        self.trades_count = 0
        self.macd_on_won = []
        self.macd_on_lost = []
