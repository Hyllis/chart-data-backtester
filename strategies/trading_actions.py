class TradingActions:
    FEES = 0.09
    MAX_LOSS = 0.01

    def log_action(self, action, quantity, price_target, price_base):
        print("         \033[34;1m{}\033[0m \033[1m{:.8f} - \033[34;1m{}\033[0m \033[1m{:.8f} | {} {:.8f} at {:.8f}, for {:.8f}"
              .format('USDT', self.wallet['usdt'], 'OTH', self.wallet['other'], action, quantity, price_target, price_base))

    def log(self, is_won, value, entry, stop_loss, take_profit):
        won = '\033[32;1mwon\033[0m' if is_won else '\033[31;1mlost\033[0m'
        print("\033[33;1m{}\033[0m \033[1m| \033[34;1m{}\033[0m \033[1m{:.8f} - \033[34;1m{}\033[0m \033[1m{:.8f} | {} {:.2f} | {:.8}, {:.8}, {:.8} | {}"
              .format('Wallet', 'USDT', self.wallet['usdt'], 'OTH', self.wallet['other'], won, value, entry, stop_loss, take_profit, self.cash_flow))

    def __init__(self, name, risk_ratio, start_usdt):
        self.risk_ratio = risk_ratio
        self.strategy_name = name
        self.start_wallet = start_usdt
        self.wallet = {'usdt': start_usdt, 'other': 0}
        self.trades_count = 0
        self.won_trades = 0
        self.won_unworthy_trades = 0
        self.unworthy_trades = 0
        self.current_streak_win = 0
        self.current_streak_lose = 0
        self.max_streak_win = 0
        self.max_streak_lose = 0
        self.gains = ["0"]
        self.all_gains = []
        self.all_loses = []
        self.loss_near_risk_ratio = []
        self.above_one = 0
        self.above_one_lost = 0
        self.cash_flow = 0

    def buy(self, entry, stop_loss, take_gain, trade, ratio):
        risked_base_currency = self.wallet['usdt'] * TradingActions.MAX_LOSS  # wallet 10,000 USD --> 100
        on_losing_trade = stop_loss / entry - 0.00000001  # sl 97 - entry 100 --> 0.97
        on_winning_trade = take_gain / entry  # tp 103.4 - entry 100 --> 1.034
        trade['buy_in'] = (risked_base_currency / (1 - on_losing_trade)) * ratio  # on_losing_trade=0.97 risked_currency_loss=100 --> 3333.333
        # print("[BUY Adding to cahs flow] {} - total {}".format(trade['buy_in'], self.cash_flow))
        if trade['buy_in'] > self.wallet['usdt']:
            trade['buy_in'] = self.wallet['usdt']
        self.cash_flow += trade['buy_in']
        # print("on losing trade: {:.4f} - on winning trade: {:.4f}  - buyin {} - risked {}".format(on_losing_trade, on_winning_trade, buy_in, risked_base_currency))
        raw_bought_target_currency = trade['buy_in'] / entry
        bought_target_currency = raw_bought_target_currency * (1 - (TradingActions.FEES / 100 * 2))
        if 'start_wallet' not in trade:
            trade['start_wallet'] = self.wallet['usdt']
        self.wallet['usdt'] -= trade['buy_in']
        self.wallet['other'] += bought_target_currency
        if ratio > 0:
            self.log_action('bought', raw_bought_target_currency, entry, trade['buy_in'])

    def sell(self, selling_price, trade, ratio):
        target = self.wallet['other'] * ratio
        raw_sold_base_currency = target * selling_price
        trade['sold_base_currency'] = raw_sold_base_currency * (1 - (TradingActions.FEES / 100 * 2))
        self.wallet['usdt'] += trade['sold_base_currency']
        trade['end_wallet'] = self.wallet['usdt']
        self.cash_flow += trade['sold_base_currency']
        # print("[SELL Adding to cahs flow] {} - total {}".format(trade['sold_base_currency'], self.cash_flow))
        self.wallet['other'] -= target
        self.log_action('sold', target, selling_price, raw_sold_base_currency)
        trade['finalized'] = ratio == 1.0

        if trade['finalized']:
            self.trades_count += 1
            trade['won_global'] = trade['end_wallet'] > trade['start_wallet']
            profit = abs(trade['end_wallet'] - trade['start_wallet'])

            if trade['won_global']:
                self.won_trades += 1
                self.all_gains.append(profit)
                # if unworthy:
                #     self.won_unworthy_trades += 1
                self.current_streak_lose = 0
                self.current_streak_win += 1
                if self.current_streak_win > self.max_streak_win:
                    self.max_streak_win = self.current_streak_win
            else:
                # proximity_ratio = (status['max_period'] - entry) / (take_gain - entry) * self.risk_ratio
                # if proximity_ratio > 1:
                #     self.above_one += 1
                #     self.above_one_lost += abs(buy_in - sold_base_currency)
                # print("proximity " + str(proximity_ratio))
                # self.loss_near_risk_ratio.append(proximity_ratio)
                self.all_loses.append(profit)
                self.current_streak_win = 0
                self.current_streak_lose += 1
                if self.current_streak_lose > self.max_streak_lose:
                    self.max_streak_lose = self.current_streak_lose


    def register_trade(self, entry, stop_loss, take_gain, status):
        unworthy = False
        risked_base_currency = self.wallet['usdt'] * TradingActions.MAX_LOSS  # wallet 10,000 USD --> 100
        on_losing_trade = stop_loss / entry  # sl 97 - entry 100 --> 0.97
        if on_losing_trade > 0.99:
            self.unworthy_trades += 1
            return
        on_winning_trade = take_gain / entry  # tp 103.4 - entry 100 --> 1.034
        buy_in = risked_base_currency / (1 - on_losing_trade)  # on_losing_trade=0.97 risked_currency_loss=100 --> 3333.333
        if buy_in > self.wallet['usdt']:
            buy_in = self.wallet['usdt']
            print("unworthy trade")
            unworthy = True
            self.unworthy_trades += 1
        print("on losing trade: {:.4f} - on winning trade: {:.4f}  - buyin {} - risked {}".format(on_losing_trade, on_winning_trade, buy_in, risked_base_currency))
        raw_bought_target_currency = buy_in / entry
        bought_target_currency = raw_bought_target_currency * (1 - (TradingActions.FEES / 100 * 2))
        self.wallet['usdt'] -= buy_in
        self.log_action('bought', raw_bought_target_currency, entry, buy_in)

        selling_price = take_gain if status['won'] else stop_loss
        raw_sold_base_currency = bought_target_currency * selling_price
        sold_base_currency = raw_sold_base_currency * (1 - (TradingActions.FEES / 100 * 2))
        self.wallet['usdt'] += sold_base_currency
        self.log_action('sold', bought_target_currency, selling_price, raw_sold_base_currency)

        self.log(status['won'], abs(buy_in - sold_base_currency), entry, stop_loss, take_gain)

        p = sold_base_currency - buy_in
        p = ("+" if p >= 0 else "") + str(p)
        print("pushed " + p + " - " + str(self.wallet['usdt']))
        self.gains.append(p)

        self.trades_count += 1
        if status['won']:
            self.won_trades += 1
            self.all_gains.append(abs(buy_in - sold_base_currency))
            if unworthy:
                self.won_unworthy_trades += 1
            self.current_streak_lose = 0
            self.current_streak_win += 1
            if self.current_streak_win > self.max_streak_win:
                self.max_streak_win = self.current_streak_win
        else:
            proximity_ratio = (status['max_period'] - entry) / (take_gain - entry) * self.risk_ratio
            if proximity_ratio > 1:
                self.above_one += 1
                self.above_one_lost += abs(buy_in - sold_base_currency)
            print("proximity " + str(proximity_ratio))
            self.loss_near_risk_ratio.append(proximity_ratio)
            self.all_loses.append(abs(buy_in - sold_base_currency))
            self.current_streak_win = 0
            self.current_streak_lose += 1
            if self.current_streak_lose > self.max_streak_lose:
                self.max_streak_lose = self.current_streak_lose

    def finish(self):
        print("\n\033[34;1m{}\033[0m \033[1m({} risk ratio)\033[0m".format(self.strategy_name, self.risk_ratio))
        print("- \033[1mWin ratio:\033[0m {}/{} ({:.2f}%)".format(self.won_trades, self.trades_count, 0 if self.trades_count == 0 else self.won_trades / self.trades_count * 100))
        print("- \033[1mUnworthy trades:\033[0m {} ({} won)".format(self.unworthy_trades, self.won_unworthy_trades))
        print("- \033[1mWallet start:\033[0m {:.2f}".format(self.start_wallet))
        print("- \033[1mWallet end:\033[0m {}{:.2f}\033[0m".format("\033[31;1m" if self.wallet['usdt'] < self.start_wallet else "\033[32;1m", self.wallet['usdt']))
        print("- \033[1mMax streak win:\033[0m \033[32;1m{}\033[0m".format(self.max_streak_win))
        print("- \033[1mMax streak lose:\033[0m \033[31;1m{}\033[0m".format(self.max_streak_lose))
        print("Average gains: {:.2f} / loses: {:.2f}".format(0 if len(self.all_gains) == 0 else sum(self.all_gains) / len(self.all_gains), 0 if len(self.all_loses) == 0 else sum(self.all_loses) / len(self.all_loses)))
        # print("Average proximity: {:.4f}".format(sum(self.loss_near_risk_ratio) / len(self.loss_near_risk_ratio)))
        print("Cash flow: {:.8f} USDT".format(self.cash_flow))
        print("Above one: {} - {}".format(self.above_one, self.above_one_lost))
        print(" ".join(self.gains))
