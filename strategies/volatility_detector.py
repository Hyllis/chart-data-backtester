from indicators.indicator import Indicator
from indicators.candles.average_true_range import AverageTrueRange
from indicators.values.exponential_moving_average_values import ExponentialMovingAverageValues
from random import random

class VolatilityDetector(Indicator):
    indicators = [
        AverageTrueRange,
    ]

    @staticmethod
    def indicator_from_type(indicator_type: str):
        filtered = list(filter(lambda indicator: indicator.ontology()['type'] == indicator_type, VolatilityDetector.indicators))
        if len(filtered) != 1:
            raise Exception("Unknown indicator: {}".format(indicator_type))
        return filtered[0]

    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'Volatility',
            'params': [
                {'name': 'indicator_id', 'type': str, 'default': 'ATR'},
                {'name': 'indicator_params', 'type': str, 'default': '14'},
            ],
        }

    def next(self, candle):
        volatility = None
        if self.params.indicator_id == 'ATR':
            atr = self.indicator.next(candle)['value']
            emav = self.emav.next(atr)['value']
            volatility = 1 if atr is not None and emav is not None and atr > emav else 0
        elem = {'value': volatility}
        self.current = elem.copy()
        return elem

    def __init__(self, params):
        super().__init__(params)
        indicator_params = self.params.indicator_params.split("|")
        self.indicator = VolatilityDetector.indicator_from_type(self.params.indicator_id)(indicator_params)
        self.emav = ExponentialMovingAverageValues([200])
