from indicators.indicator import Indicator
from indicators.candles.exponential_moving_average import ExponentialMovingAverage
from indicators.candles.macd import MACD
from indicators.candles.period_min_max import PeriodMinMax
from indicators.candles.bollinger_bands import BollingerBands


class Bollinger(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'Bollinger',
            'params': [
                {'name': 'risk_ratio', 'type': float, 'default': 1.4, 'range': {'min': 1}},
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        print("risk ratio: {}".format(self.params.risk_ratio))
        self.i = -1
        self.ema_trend = ExponentialMovingAverage([200, 'close'])
        self.min_max = PeriodMinMax([50])
        self.bb = BollingerBands([20, 2])

    def update_trade(self, candle, trade):
        return {}

    def next(self, candle):
        self.i += 1
        elem = {}
        ema_trend = self.ema_trend.next(candle)['value']
        trend_up = None if ema_trend is None else candle['close'] > ema_trend
        min_max = self.min_max.next(candle)
        bb = self.bb.next(candle)
        if trend_up and bb['lower'] is not None and candle['low'] < bb['lower'] < candle['close']:
            entry = candle['close']
            take_profit = min_max['max']
            stop_loss = candle['close'] - (take_profit - entry) / self.params.risk_ratio
            # stop_loss = candle['low']  # min(ema_trend, min_max['min'])
            # take_profit = entry + (entry - stop_loss) * self.params.risk_ratio
            elem = {'date': candle['date'], 'entry': entry,
                    'type': 'SHORT', 'stop_loss': take_profit, 'take_profit': stop_loss,
                    'buy_ratio': 1, 'sell_ratio': 1, 'initial': True}
        self.current = elem.copy()
        return elem
