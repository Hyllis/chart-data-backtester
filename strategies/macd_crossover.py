from indicators.indicator import Indicator
from indicators.candles.exponential_moving_average import ExponentialMovingAverage
from indicators.candles.macd import MACD
from indicators.candles.period_min_max import PeriodMinMax


class MACDCrossover(Indicator):
    @classmethod
    def ontology(cls):
        return {
            'name': cls.__name__,
            'type': 'MACDCrossover',
            'params': [
                {'name': 'risk_ratio', 'type': float, 'default': 1.4, 'range': {'min': 1}},
            ],
        }

    def __init__(self, params):
        super().__init__(params)
        print("risk ratio: {}".format(self.params.risk_ratio))
        self.i = -1
        self.ema_trend = ExponentialMovingAverage([200, 'close'])
        self.macd = MACD([12, 26, 'close', 9])
        self.macd_above_prev = None
        self.min_max = PeriodMinMax([50])

    @staticmethod
    def macd_crossing_direction(value, prev_value):
        if value is None or prev_value is None:
            return 0
        if value and not prev_value:  # crossed up - long
            return 1
        if prev_value and not value:  # crossed down - short
            return -1
        return 0

    def update_trade(self, candle, trade):
        elem = {}
        stop_loss_step = trade['entry'] - trade['stop_loss']
        tp1 = trade['entry'] + stop_loss_step
        if trade['initial'] and candle['close'] > tp1:
            trade['take_profit'] = tp1
            trade['sell_ratio'] = 0.25
            sw = trade['start_wallet']
            trade['start_wallet'] = None
            tp2 = trade['entry'] + stop_loss_step * 2
            elem = {'date': candle['date'], 'entry': candle['close'], 'type': 'LONG',
                    'stop_loss': trade['entry'], 'take_profit': tp2, 'macd': trade['macd'],
                    'buy_ratio': 0, 'sell_ratio': 1, 'start_wallet': sw, 'initial': False}
        return elem

    def next(self, candle):
        self.i += 1
        ema_trend = self.ema_trend.next(candle)['value']
        min_max = self.min_max.next(candle)
        trend_up = None if ema_trend is None else candle['close'] > ema_trend
        macd = self.macd.next(candle)
        macd_above = None if macd['macd'] is None or macd['signal'] is None else macd['macd'] > macd['signal']
        macd_crossed = MACDCrossover.macd_crossing_direction(macd_above, self.macd_above_prev)
        self.macd_above_prev = macd_above
        hist_distance = 0 if macd['macd'] is None else macd['macd']  # < 0 = long
        elem = {}
        min_close_max_ratio = 1 if min_max['max'] - min_max['min'] == 0 else 1 - ((min_max['max'] - candle['close']) / (min_max['max'] - min_max['min']))
        if min_close_max_ratio < 0.6 and trend_up and macd_crossed > 0 and hist_distance < 0 and macd['macd'] / candle['close'] * 100 < -0.1:
            entry = candle['close']
            # stop_loss = ema_trend
            stop_loss = min(ema_trend, min_max['min'])
            take_profit = entry + (entry - stop_loss) * self.params.risk_ratio
            elem = {'date': candle['date'], 'entry': entry, 'type': 'LONG',
                    'stop_loss': stop_loss, 'take_profit': take_profit, 'macd': macd['macd'],
                    'buy_ratio': 1, 'sell_ratio': 1, 'initial': True}
        self.current = elem.copy()
        return elem
