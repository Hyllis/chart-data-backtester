from abc import abstractmethod


class PatternInterface:
    @classmethod
    def name(cls):
        return cls.__name__

    @staticmethod
    @abstractmethod
    def direction():
        pass  # bullish - bearish - continuation

    @staticmethod
    def strength():
        return 1

    @staticmethod
    @abstractmethod
    def detect(trend, a, b, c, d, e):
        pass

    @staticmethod
    def get_candle_details(candle):
        green = candle['close'] > candle['open']
        top = candle['close'] if green else candle['open']
        bottom = candle['open'] if green else candle['close']
        upper_shadow = candle['high'] - top
        body = top - bottom + 0.00000001
        lower_shadow = bottom - candle['low']
        return {
            'green': green,
            'upper_shadow': upper_shadow,
            'body': body,
            'lower_shadow': lower_shadow,
            'upper_ratio': upper_shadow / body,
            'lower_ratio': lower_shadow / body,
        }

