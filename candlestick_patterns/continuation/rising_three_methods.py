from candlestick_patterns.pattern_interface import PatternInterface


class RisingThreeMethods(PatternInterface):
    @staticmethod
    def direction():
        return 'CONTINUATION'

    @staticmethod
    def detect(trend, candle_outer_right, candle_right, candle_center, candle_left, candle_outer_left):
        return False
