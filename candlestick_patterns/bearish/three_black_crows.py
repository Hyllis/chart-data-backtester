from candlestick_patterns.pattern_interface import PatternInterface


class ThreeBlackCrows(PatternInterface):
    @staticmethod
    def direction():
        return 'BEARISH'

    @staticmethod
    def strength():
        return 1.5

    @staticmethod
    def detect(trend, candle_right, candle_center, candle_left, d, e):
        if trend < 1:  # downtrend only
            return False
        left = PatternInterface.get_candle_details(candle_left)
        center = PatternInterface.get_candle_details(candle_center)
        right = PatternInterface.get_candle_details(candle_right)
        if left['green'] or center['green'] or right['green']:
            return False
        max_ratio = 1
        if left['upper_ratio'] + left['lower_ratio'] > max_ratio:
            return False
        if center['upper_ratio'] + center['lower_ratio'] > max_ratio:
            return False
        if right['upper_ratio'] + right['lower_ratio'] > max_ratio:
            return False
        return True
