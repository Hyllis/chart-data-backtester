from candlestick_patterns.pattern_interface import PatternInterface


class EveningStar(PatternInterface):
    @staticmethod
    def direction():
        return 'BEARISH'

    @staticmethod
    def detect(trend, candle_right, candle_center, candle_left, d, e):
        if trend < 1:  # uptrend only
            return False
        left = PatternInterface.get_candle_details(candle_left)
        center = PatternInterface.get_candle_details(candle_center)
        right = PatternInterface.get_candle_details(candle_right)
        if not left['green'] or right['green']:
            return False
        body_min = min(left['body'], right['body'])
        body_max = max(left['body'], right['body'])
        return center['body'] / left['body'] < 0.25 and body_min / body_max > 0.8
