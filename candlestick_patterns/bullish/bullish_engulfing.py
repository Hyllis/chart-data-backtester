from candlestick_patterns.pattern_interface import PatternInterface


class BullishEngulfing(PatternInterface):
    MINIMUM_LEFT_TO_RIGHT_RATIO = 0.6
    MINIMUM_BODY_TO_SHADOW_RATIO = 1

    @staticmethod
    def direction():
        return 'BULLISH'

    @staticmethod
    def strength():
        return 0.3

    @staticmethod
    def detect(trend, candle_right, candle_left, c, d, e):
        if trend > -1:  # downtrend only
            return False
        left = PatternInterface.get_candle_details(candle_left)
        right = PatternInterface.get_candle_details(candle_right)
        if left['green'] or not right['green']:
            return False
        if left['upper_ratio'] > BullishEngulfing.MINIMUM_BODY_TO_SHADOW_RATIO or left['lower_ratio'] > BullishEngulfing.MINIMUM_BODY_TO_SHADOW_RATIO:
            return False
        if right['upper_ratio'] > BullishEngulfing.MINIMUM_BODY_TO_SHADOW_RATIO or right['lower_ratio'] > BullishEngulfing.MINIMUM_BODY_TO_SHADOW_RATIO:
            return False
        return left['body'] / right['body'] < BullishEngulfing.MINIMUM_LEFT_TO_RIGHT_RATIO
