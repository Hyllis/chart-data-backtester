from candlestick_patterns.pattern_interface import PatternInterface


class StrictHammer(PatternInterface):
    @staticmethod
    def direction():
        return 'BULLISH'

    @staticmethod
    def detect(trend, candle, b, c, d, e):
        if trend > -1:  # downtrend only
            return False
        data = PatternInterface.get_candle_details(candle)
        return data['lower_ratio'] > 2 and data['upper_ratio'] < 0.2
