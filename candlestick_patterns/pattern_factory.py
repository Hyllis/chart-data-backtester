from candlestick_patterns.bullish.hammer import Hammer
from candlestick_patterns.bullish.strict_hammer import StrictHammer
from candlestick_patterns.bullish.big_hammer import BigHammer
from candlestick_patterns.bullish.mega_hammer import MegaHammer
from candlestick_patterns.bullish.inverted_hammer import InvertedHammer
from candlestick_patterns.bullish.bullish_engulfing import BullishEngulfing
from candlestick_patterns.bullish.morning_star import MorningStar
from candlestick_patterns.bullish.three_white_soldiers import ThreeWhiteSoldiers
from candlestick_patterns.bearish.hanging_man import HangingMan
from candlestick_patterns.bearish.strict_hanging_man import StrictHangingMan
from candlestick_patterns.bearish.big_hanging_man import BigHangingMan
from candlestick_patterns.bearish.mega_hanging_man import MegaHangingMan
from candlestick_patterns.bearish.shooting_star import ShootingStar
from candlestick_patterns.bearish.bearish_engulfing import BearishEngulfing
from candlestick_patterns.bearish.evening_star import EveningStar
from candlestick_patterns.bearish.three_black_crows import ThreeBlackCrows
from candlestick_patterns.continuation.rising_three_methods import RisingThreeMethods


class Pattern:
    patterns = [  # returns on the first match; the order is important
        # Bullish
        MegaHammer,
        BigHammer,
        StrictHammer,
        Hammer,
        InvertedHammer,
        MorningStar,
        BullishEngulfing,
        ThreeWhiteSoldiers,

        # Bearish
        MegaHangingMan,
        BigHangingMan,
        StrictHangingMan,
        HangingMan,
        ShootingStar,
        EveningStar,
        BearishEngulfing,
        ThreeBlackCrows,

        # Continuation
        RisingThreeMethods,
    ]

    @staticmethod
    def pattern_from_name(pattern_name: str):
        filtered = list(filter(lambda pattern: pattern.name() == pattern_name, Pattern.patterns))
        if len(filtered) != 1:
            raise Exception("Unknown pattern: {}".format(pattern_name))
        return filtered[0]

    @staticmethod
    def detect(pattern_name: str, trend, candles):
        if len(candles) < 5:
            return {}
        pattern = Pattern.pattern_from_name(pattern_name)
        detected = pattern.detect(trend, candles[4], candles[3], candles[2], candles[1], candles[0])
        if detected:
            print("found pattern " + pattern.name() + ' - ' + pattern.direction() + " - " + str(detected))
            return {'detected': {'name': pattern.name(), 'direction': pattern.direction(), 'strength': pattern.strength()}}
        else:
            return {}

    @staticmethod
    def detect_any(trend, candles):
        if len(candles) < 5:
            return {}
        for pattern in Pattern.patterns:
            detected = pattern.detect(trend, candles[4], candles[3], candles[2], candles[1], candles[0])
            if detected:
                return {'detected': {'name': pattern.name(), 'direction': pattern.direction(), 'strength': pattern.strength()}}
        return {}
