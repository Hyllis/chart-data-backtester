from models.period import Period
from models.currency import Currency
from database_connector import DatabaseConnector
from indicators.factory import Factory
import math


class ChartData:
    PAGE_SIZE = 20000

    def __init__(self, base: Currency, target: Currency, period: Period):
        self.base = base
        self.target = target
        self.period = period
        self.connector = DatabaseConnector(base, target).set_period(period)
        self.candles = self.connector.get_candles()
        self.indicators = {}

    def get_candles(self):
        return self.candles

    def get_candles_page(self, page_id: int):
        max_page = math.ceil(len(self.candles) / ChartData.PAGE_SIZE) - 1
        if page_id == 'last':
            page_id = max_page
        from_id = 0 if page_id == 'all' else int(page_id) * ChartData.PAGE_SIZE
        to_id = 9999999999 if page_id == 'all' else from_id + ChartData.PAGE_SIZE
        pagination = {'total': len(self.candles),
                      'current_page': page_id,
                      'max_page': max_page}
        return {'pagination': pagination, 'values': self.candles[from_id:to_id]}

    def get_indicator_data(self, indicator: str):
        # return Factory.get(indicator, self.candles, self.indicators).get_data()
        if indicator not in self.indicators:
            self.indicators[indicator] = Factory.get(indicator, self.indicators)
        return self.indicators[indicator].get_data(self.candles)

    def get_indicator_data_page(self, indicator: str, page_id: int):
        indicator_data = self.get_indicator_data(indicator)
        max_page = math.ceil(len(indicator_data) / ChartData.PAGE_SIZE) - 1
        if page_id == 'last':
            page_id = max_page
        from_id = 0 if page_id == 'all' else int(page_id) * ChartData.PAGE_SIZE
        to_id = 9999999999 if page_id == 'all' else from_id + ChartData.PAGE_SIZE
        pagination = {'total': len(indicator_data),
                      'current_page': page_id,
                      'max_page': max_page}
        return {
            'pagination': pagination,
            'data': indicator_data[from_id:to_id]
        }
