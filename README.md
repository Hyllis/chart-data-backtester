# Demo

[![Trading view demo](http://img.youtube.com/vi/h925vetjE6E/0.jpg)](http://www.youtube.com/watch?v=h925vetjE6E "Trading view demo")

# Basics

This project allows you to use data fetched with Poloniex API to **backtest trading strategies**, analyze the results, and **visualize** them in a browser. The visualizer allows you to update any parameters (stategies & simple indicators) in real-time.

There's three projects:

- [chart-data-fetcher](https://bitbucket.org/Hyllis/chart-data-fetcher): fetches candle data from the Poloniex API, and inserts them in a MySQL database;
- [chart-data-backtester](https://bitbucket.org/Hyllis/chart-data-backtester/): REST resources used by the visualizer – pull data from the database and runs any indicators required;
- [chart-data-visualizer](https://bitbucket.org/Hyllis/chart-data-visualizer/): displays the data in your browser – add/remove indicators and edit their parameters.

# Setup

Tested on Debian-based Linux only, but it should work on other platforms as well.

### Database

- Setup a MySQL server locally;
- create a database named `cryptodata`;
- add a user `crypto` identified by password `helloworld`;
- grant them the permissions to `cryptodata`.

Of course, feel free to setup your database differently *(I was running it in a VM on my NAS, to keep the data updated at all times)*. You'll need to update `database_connector.py`, both on the [fetcher](https://bitbucket.org/Hyllis/chart-data-fetcher) and [backtester](https://bitbucket.org/Hyllis/chart-data-backtester).

### Fill the database (fetcher)

We'll use [chart-data-fetcher](https://bitbucket.org/Hyllis/chart-data-fetcher/) to feed the database with data from Poloniex; follow the instruction in [setup.sh](https://bitbucket.org/Hyllis/chart-data-fetcher/src/master/setup.sh) to activate a venv and install a few Python packages.

Check the file [run.py](https://bitbucket.org/Hyllis/chart-data-fetcher/src/master/run.py) to define which **currency pair** you'd like to track. (There's already a dozen by default.)

By default, the pairs are dumped from **2017-01-01**, for **every time frame** available on Poloniex (daily, 4/2 hours, 30/15/5 minutes). Visit [dump_pair.py](https://bitbucket.org/Hyllis/chart-data-fetcher/src/master/dump_pair.py) to change that.

Finally, with the venv activated, run:
```
python3 run.py
```
Looping the execution would **keep your data updated**, as if data are found in the database for a currency pair, it would only fetch from there.

### Run the backtester

Back to this current project: follow the instruction in [setup.sh](https://bitbucket.org/Hyllis/chart-data-backtester/src/master/setup.sh) to activate a venv and install a few Python packages.

Enable the venv, then:
```
python3 chart_data_resource.py
```


### Visualize

Now, open [chart-data-visualizer](https://bitbucket.org/Hyllis/chart-data-visualizer/)'s `index.html` in your browser, and everything should work!

The default currency pair and timeframe are set in [conf.js](https://bitbucket.org/Hyllis/chart-data-visualizer/src/master/conf.js). The indicators enabled by default are set in [data/candles.js](https://bitbucket.org/Hyllis/chart-data-visualizer/src/master/data/candles.js).

# Add an indicator / strategy

### The indicator itself

Head off to [chart-data-backtester](https://bitbucket.org/Hyllis/chart-data-backtester/) and check the folder `indicators/candles`; then pick a file, say, [bollinger_bands.py](https://bitbucket.org/Hyllis/chart-data-backtester/src/master/indicators/candles/bollinger_bands.py).

- `ontology` defines so basic data, like its `type` (an ID used to load it), then the required `params` to use it.
- `next` is run on every candle stored for the selected curency pair. Only one candle at a time, to make it easy to convert to a live trading bot!

It's entirely possible to use other existing indicators to build another one. For instance, Bollinger Bands are computed use a moving average and a standard deviation; both standalone indicators.

**Note:** indicators are cached when access from the REST endpoint (from the visualizer): loading two indicators with the same parameters won't re-compute the values. However, this system is not implemented for indicators use from another indicator. For instance, if two indicators needs a moving average on the same data with the same parameters, it will be computed twice.

### Indicator factory

To make in available from the REST API:

- open [indicators/factory.py](https://bitbucket.org/Hyllis/chart-data-backtester/src/master/indicators/factory.py);
- import your indicator;
- add it to the `indicators` array.

The `type` you provided in your indacator's `ontology` will be used here.

### In the visualizer

And the biggest thing I wanted to change with the TypeScript version of the visualizer I wanted to make: you have to partially duplicate in the visualizer the data you entered in your indicator's `ontology`.

Open [indicators.js](https://bitbucket.org/Hyllis/chart-data-visualizer/src/master/indicators.js), and search for the object `indicatorTemplates`.

The keys are equal to the `type` of your indicator in the backtester.

- `name` is the displayed name when adding a new indicator in the view.
- `params` should normally contains the same params you indicated in the `ontology` of your indicator.
- `display` is the visual representation of your indicator.
- `display.external` define if this indicator should be shown outside of the main canvas (with the candles).
- `display.charts` contains the different data points of your indicator. Those keys should be the same as the ones returned in the `next` method of your indicator in the backtester. The exception being the `polygon` key, used to draw an area between to data series (like the semi-transparent area of the Bollinger Bands).

The chart type can be `simple` (lines like a moving average), `histogram`, etc. You can view the types in [display/display-charts.js](https://bitbucket.org/Hyllis/chart-data-visualizer/src/master/display/display-charts.js) (`displayIndicators()`), or add a new one, but this part is a mess!

# Conclusion

This should enough to use it and modify it to your needs, if you're interesting in cryptocurrency trading and backtesting!

With some minor tweaks to [strategies/strategy_executor.py](https://bitbucket.org/Hyllis/chart-data-backtester/src/master/strategies/strategy_executor.py), it should be relatively straightforward to turn it into a live trading bot, using the Polonix API Python module used in the [chart-data-fetcher](https://bitbucket.org/Hyllis/chart-data-fetcher/) to fetch candle data, after adding your Poloniex API private key.

If any question, hit me up!
