from models.currency import Currency
from models.period import Period
from models.candle import Candle
import mysql.connector
from datetime import datetime


class DatabaseConnector:
    def __init__(self, base: Currency, target: Currency):
        self.base = base
        self.target = target
        self.period = Period.DAILY
        self.table_name = base.name + "-" + target.name + "_" + str(self.period.value)
        self.mydb = mysql.connector.connect(host="127.0.0.1", port=3306,
                user="crypto", passwd="helloworld", database="cryptodata",
                auth_plugin='mysql_native_password')
        self.mycursor = self.mydb.cursor()

    def set_period(self, period: Period):
        self.period = period
        self.table_name = self.base.name + "-" + self.target.name + "_" + str(period.value)
        return self

    def get_candles_from(self, from_date: str):
        try:
            self.mycursor.execute("SELECT * FROM `{}` WHERE date >= '{}'".format(self.table_name, from_date))
            candles = []
            for db_candle in self.mycursor.fetchall():
                candle = Candle.tuple_to_dict(db_candle)
                # candle.show()
                candles.append(candle)
            return candles
        except mysql.connector.Error as err:
            print("Error: {}".format(err))

    def get_candles(self):
        return self.get_candles_from("2000-01-01")

    def get_table_size(self):
        try:
            self.mycursor.execute("SELECT COUNT(*) FROM `{}`".format(self.table_name))
            return int(self.mycursor.fetchone()[0])
        except mysql.connector.Error as err:
            print("Error: {}".format(err))
            return -1

    def get_last_date(self):
        try:
            self.mycursor.execute("SELECT * FROM `{}` ORDER BY date DESC LIMIT 1".format(self.table_name))
            res = self.mycursor.fetchone()
            return None if res is None else res[0]
        except mysql.connector.Error as err:
            print("Error: {}".format(err))
            return datetime.utcfromtimestamp(1483228800)  # 2017-01-01

    def insert_candle(self, candle):
        dt = datetime.utcfromtimestamp(candle.date)
        query = "INSERT INTO `{}` (date, open, high, low, close, volume, quoteVolume, weightedAverage) VALUES ('{}', {}, {}, {}, {}, {}, {}, {})"\
            .format(self.table_name, dt, candle.open, candle.high, candle.low, candle.close, candle.volume, candle.quoteVolume, candle.weightedAverage)
        if candle.date == 0:
            print("wut " + candle.open + " - " + query)
        try:
            # print("ts: " + str(candle.date))
            self.mycursor.execute(query)
            print(query)
        except mysql.connector.Error as err:
            if "Duplicate entry" not in str(err):
                print("Error: {}".format(err))

    def insert_candles(self, chart_data):
        for candle in chart_data:
            self.insert_candle(candle)
        self.commit()

    def commit(self):
        self.mydb.commit()
